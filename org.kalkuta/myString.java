public class myString {
    public static void main(String[] args) {

        String str  = "Hello World! My name is App.";

        StringBuffer sbr = new StringBuffer(str.toLowerCase());
        sbr.reverse();
        for(int i=1; i<=sbr.length()-1; i=i+2) sbr.setCharAt(i, Character.toUpperCase(sbr.charAt(i)));

        System.out.println(sbr);
        //        .PpA Si eMaN Ym !DlRoW OlLeH
    }
}
